export default {
    data: {
        title: "",
    //定义菜单项的数据源
        menus:[{"text":"房间","inActiveImg":"common/images/home.png","activeImg":"common/images/home_active.png",showBadge:true,msgNum:"5"},
               {"text":"设备","inActiveImg":"common/images/device.png","activeImg":"common/images/device_active.png"},
               {"text":"消息","inActiveImg":"common/images/msg.png","activeImg":"common/images/msg_active.png",showBadge:true,msgNum:"100"},
               {"text":"设置","inActiveImg":"common/images/settings.png","activeImg":"common/images/settings_active.png"}],

    },
    //响应导航栏菜单切换事件
    changePage(e){
        this.title = e.detail.index;
        console.error("title="+this.title);
    },
    onInit() {
        this.title = this.$t('strings.world');
    }
}
