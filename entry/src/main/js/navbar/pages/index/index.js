export default {
    props: {
        menuData: [], //定义菜单项的数据属性，菜单内容由该数据生成
        defaultIndex: { //菜单的默认索引
            default: 0
        },
        itemTextActiveColor: { //菜单选中的时候的文本颜色
            default: '#1a1aff'
        },
        itemTextInActiveColor: { //菜单非选中的时候的文本颜色
            default: '#bfbfbf'
        },
        backgroundColor: { //导航栏的背景色
            default: '#ffffff'
        },
        borderTopWidth: { //底部导航栏顶部的一条分割线宽度，为0代表没有
            default: '2px'
        },
        borderBottomWidth: { //顶部导航栏顶部的一条分割线宽度，为0代表没有
            default: '2px'
        },
        borderColor: { //导航栏分割线颜色
            default: '#bfbfbf'
        },
        isBottom: {     //是否是底部导航栏
            default: true
        },
        badgeconfig: {
            default: {
                badgeColor: "#fa2a2d",
                textColor: "#ffffff",
            }
        }
    },
    data: {
        bottom: null
    },
//自定义事件eventNavItemChange的响应
    changemenu(index)
    {
        //赋值
        this.defaultIndex = index;
        console.log("当前的值为:" + this.defaultIndex);
        this.$emit('eventNavItemChange', {
            index: index
        });
    },
    onInit() {
        if (this.isBottom) { //设置为底部导航栏
            this.bottom = '0px';
            this.borderBottomWidth = '0px'; //底部导航栏只有上分隔线没有下分隔线
        } else { //设置为顶部部导航栏
            this.bottom = null;
            this.borderTopWidth = '0px'; //顶部导航栏只有下分隔线没有上分隔线
        }

    }
}