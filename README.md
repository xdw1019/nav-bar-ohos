# NavBarOhos
## 简介
基于OpenHarmony SDK开发封装自定义组件，该组件为应用开发中非常常用的TAB导航栏，默认为底部导航栏，也可以设置为顶部导航栏，同时支持红点和数字提醒两种角标提醒。

## 项目工程结构

![img](readmeImg/01.png)

## 运行效果图

### 底部导航栏效果图

![img](readmeImg/02.gif)

### 顶部导航栏效果图

![img](readmeImg/03.gif)

### 设置导航栏背景色和文字激活色效果图

![img](readmeImg/04.gif)

## 使用示例：

1.下载组件代码navbar目录到本地, 添加到你的项目的js目录下

2.通过element引入navbar组件

```
<!--start 引入导航栏组件 start-->
<element name='comp' src='../../../navbar/pages/index/index.hml'></element>
<!--end 引入导航栏组件 end-->
<!--start 引入需要嵌套的子页面组件 start-->
<element name='page1' src='../page1/page1.hml'></element>
<element name='page2' src='../page2/page2.hml'></element>
<element name='page3' src='../page3/page3.hml'></element>
<element name='page4' src='../page4/page4.hml'></element>
<!--end 引入需要嵌套的子页面组件 end-->
<div class="container">
<!--start 加载导航栏组件 start-->
    <comp id = "selfDefineChild" menu-data="{{ menus }}"  default-index="1"  @event-nav-item-change="changePage" ></comp>
<!--end 加载导航栏组件 end-->
<!--start 根据导航栏菜单选中动态加载对应的内嵌子页面 start-->
    <block if="{{this.$child('selfDefineChild').defaultIndex==0}}">
        <page1></page1>
    </block>
    <block if="{{this.$child('selfDefineChild').defaultIndex==1}}">
        <page2></page2>
    </block>
    <block if="{{this.$child('selfDefineChild').defaultIndex==2}}">
        <page3></page3>
    </block>
    <block if="{{this.$child('selfDefineChild').defaultIndex==3}}">
        <page4></page4>
    </block>
<!--end 根据导航栏菜单选中动态加载对应的内嵌子页面 end-->
</div>
```

3、在js文件中定义菜单数据

```
data: {
    //定义菜单项的数据源
        menus:[{"text":"房间","inActiveImg":"common/images/home.png","activeImg":"common/images/home_active.png",showBadge:"true",msgNum:"5"},
               {"text":"设备","inActiveImg":"common/images/device.png","activeImg":"common/images/device_active.png"},
               {"text":"消息","inActiveImg":"common/images/msg.png","activeImg":"common/images/msg_active.png",showBadge:"true",msgNum:"100"},
               {"text":"设置","inActiveImg":"common/images/settings.png","activeImg":"common/images/settings_active.png"}],

},
```

## 属性说明：

| 名称                      | 类型               | 默认值                                                   | 是否必填 | 说明                                                    |
| ------------------------- | ------------------ | -------------------------------------------------------- | -------- | ------------------------------------------------------- |
| menu-data                 | [menuDataConfig](#anchor) | []                                                       | 是       | 设置底部tab的相关数据和配置属性                         |
| default-index             | number             | 0                                                        | 否       | 设置tab默认选中索引                                     |
| item-text-active-color    | string             | #1a1aff                                                  | 否       | 设置tab内选中时文字颜色                                 |
| item-text-in-active-color | string             | #bfbfbf                                                  | 否       | 设置tab内未选中时文字颜色                               |
| background-color          | string             | #ffffff                                                  | 否       | 设置导航栏背景颜色                                      |
| border-top-width          | string             | 2px                                                      | 否       | 设置底部导航栏顶部的一条分割线宽度，为0px代表没有分割线 |
| border-bottom-width       | string             | 2px                                                      | 否       | 设置顶部导航栏顶部的一条分割线宽度，为0px代表没有分割线 |
| border-color              | string             | #bfbfbf                                                  | 否       | 设置导航栏分割线颜色                                    |
| is-bottom                 | boolean            | true                                                     | 否       | 设置是否为底部导航栏，false的时候为顶部导航栏           |
| badgeconfig               | [badgeconfig](#anchor2) | {     badgeColor: "#fa2a2d",     textColor: "#ffffff", } | 否       | 设置角标提醒的背景色和文字颜色                          |

<span id = "anchor">表1 menuDataConfig</span>

| 名称        | 类型    | 默认值 | 是否必填 | 说明                                                         |
| ----------- | ------- | ------ | -------- | ------------------------------------------------------------ |
| text        | string  | -      | 是       | tab名称                                                      |
| inActiveImg | string  | -      | 是       | tab非选中时显示图片                                          |
| activeImg   | string  | -      | 是       | tab选中时显示图片                                            |
| showBadge   | boolean | -      | 否       | 是否显示tab消息角标                                          |
| msgNum      | number  | -      | 否       | tab角标上显示的消息数，为0的时候显示红点，最大为99，超过99显示为99+ |

<span id = "anchor2">表2 badgeconfig</span>

| 名称       | 类型   | 默认值  | 是否必填 | 说明             |
| ---------- | ------ | ------- | -------- | ---------------- |
| badgeColor | string | #fa2a2d | 是       | 设置角标背景颜色 |
| textColor  | string | #ffffff | 是       | 设置角标文字颜色 |

## 回调事件：

| 名称                   | 参数 | 描述              |
| ---------------------- | ---- | ----------------- |
| @event-nav-item-change | 无   | tab导航点击时触发 |